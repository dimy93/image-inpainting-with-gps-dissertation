function [ kernel_spectre ] = gaussian_viz( learned )
%GAUSSIAN_VIZ Summary of this function goes here
%   Detailed explanation goes here
kernel_spectre = zeros(512,512);
for i = 1:size(learned.mx,2)
    mu = [ learned.mx(i), learned.my(i) ];
    sigma = diag([learned.vx(i),learned.vy(i)]);
    [X1,X2] = meshgrid(1:1:512, 1:1:512);
    X = [X1(:) X2(:)];
    X = X/256;
    kernel_spectre = kernel_spectre + reshape(mvnpdf(X, mu + 1, sigma),512,512);
end
kernel_spectre = (kernel_spectre - min(min(kernel_spectre)))/max(max(kernel_spectre));
kernel_spectre = kernel_spectre*15;
end

