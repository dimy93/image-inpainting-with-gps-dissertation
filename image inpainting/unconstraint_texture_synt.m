run('../gpml/startup.m')
pic_dir = '../Normalized Brodatz/';
list = dir(strcat(pic_dir,'*.tif'));
NCCs = zeros(100,length(list));
MSSIMs = zeros(100,length(list));
TSSs = zeros(100,length(list)); 
k_init = 1;
i_init = 1;
if exist('results.mat', 'file')
   file = matfile('results.mat');
   k_init = file.k;
   NCCs = file.NCCs;
   MSSIMs = file.MSSIMs;
   TSSs = file.TSSs;
   list = file.list;
   i_init = file.i+1;
   if i_init > 20
       k_init = k_init + 1;
       i_init = 1;
   end
end
for k = k_init:length(list)
    file = 'D53.tif';
    img = im2double(imread(strcat(pic_dir,file)));
    %img = imresize(img,0.5);
    patch = img(1:size(img,1),1:size(img,2)/2);
    try
        mask = ones(size(patch));
        [~,~,hyper] = texture_synt( patch, mask, struct('Q',40, 'inits', 150, 'iters', 1000) );
        for i = 1:15
            sample = samplePriorGrid(size(patch), hyper.mean, hyper.stdev, hyper.cov, hyper.hyper);
            imwrite(sample,['sample',int2str(i),'.jpg']);
        end
        p = size(patch);
        save('hyper_params','hyper', 'p');
    catch ME
        disp(getReport(ME));
    end
end
exit;