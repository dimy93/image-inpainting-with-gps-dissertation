function [ samples ] = samplePost(texture_synt_size, texture_precond, hyper, opts)
texture_precond_size = size(texture_precond);
texture_size = texture_synt_size + texture_precond_size;
hyper.cov{3}={(1:texture_size(1))',(1:texture_size(2))'};

x = texture_precond_size(1)+1:texture_size(1);
y = texture_precond_size(2)+1:texture_size(2);
[x,y] = meshgrid(x,y);
idx_M = sub2ind(texture_size,x, y);
idx_M = idx_M';
idx_M = idx_M(:);

x = 1:texture_precond_size(1);
y = 1:texture_precond_size(2);
[x,y] = meshgrid(x,y);
idx_N = sub2ind(texture_size,x, y);
idx_N = idx_N';
idx_N = idx_N(:);

postg = infGrid(hyper.hyper, {@meanConst}, hyper.cov, @likGauss, idx_N, texture_precond(:), opts);
Knn_inv = postg.L; % calculates (K+sigma*I)^(-1)*y


hyper.cov{3}={(1:texture_size(1))',(1:texture_size(2))'};
Ks = feval(hyper.cov{:}, hyper.hyper.cov, 1:prod(texture_size));
for j=1:numel(Ks)
  if iscell(Ks{j}) && strcmp(Ks{j}{1},'toep'), Ks{j} = toeplitz(Ks{j}{2}); end
end
K = @(t)kronmvm(Ks,t);
Knm = @(t)select(K,texture_size,t,idx_N,idx_M);
Kmn = @(t)select(K,texture_size,t,idx_M,idx_N);

hyper.cov{3}={(1:texture_synt_size(1))',(1:texture_synt_size(2))'};
Ksmm = feval(hyper.cov{:}, hyper.hyper.cov, 1:prod(texture_size));
for j=1:numel(Ksmm)
  if iscell(Ksmm{j}) && strcmp(Ksmm{j}{1},'toep'), Ksmm{j} = toeplitz(Ksmm{j}{2}); end
end
Kmm = @(t)kronmvm(Ksmm,t);

K_pred = @(t)Kmm(t) - Kmn(Knn_inv(Knm(t))); % Covariance matrix of predictions
hyper.cov{3}={(1:texture_size(1))',(1:texture_size(2))'};
Knm_col = @(i)(feval(hyper.cov{:}, hyper.hyper.cov, idx_N, idx_M(i) ));
K_pred_i_j = @(i,j) indexKron(i,j,Ksmm{2},Ksmm{1})-Knm_col(i)'*Knn_inv(Knm_col(j));

samples = mvnSampling(K_pred,K_pred_i_j,texture_synt_size, 10, 500, 160);

% For test
% v = rand(prod(texture_synt_size),1);
% approx = K_pred(v);
% 
% hyper.cov{3}={(1:texture_size(1))',(1:texture_size(2))'};
% Knm_real = feval(hyper.cov{:}, hyper.hyper.cov, idx_N,idx_M );
% v_real = kron(Ksmm{2},Ksmm{1})*v-Knm_real'*Knn_inv(Knm_real*v);
% K_real = kron(Ksmm{2},Ksmm{1})-Knm_real'*Knn_inv(Knm_real);
% e1=max(max(abs(approx - v_real)));
% i = ceil(rand()*1200); j =ceil(rand()*1200);
% v_real = K_real(i,j);
% approx = K_pred_i_j(i,j);
% e2=max(max(abs(approx - v_real)));
end

function r = select(K,K_size,t,selectX,selectY)
common_size = prod(K_size);
t_padd = zeros(common_size,1);
t_padd(selectY) = t;
r_padd = K(t_padd);
r = r_padd(selectX);
end

function b = kronmvm(As,x,transp)
if nargin>2 && ~isempty(transp) && transp   % transposition by transposing parts
  for i=1:numel(As), As{i} = As{i}'; end
end
m = zeros(numel(As),1); n = zeros(numel(As),1);                  % extract sizes
for i=1:numel(n)
  if iscell(As{i}) && strcmp(As{i}{1},'toep')
    m(i) = size(As{i}{2},1); n(i) = size(As{i}{2},1);
  else [m(i),n(i)] = size(As{i});
  end
end
d = size(x,2);
b = x;
for i=1:numel(n)
  a = reshape(b,[prod(m(1:i-1)), n(i), prod(n(i+1:end))*d]);    % prepare  input
  tmp = reshape(permute(a,[1,3,2]),[],n(i))*As{i}';
  b = permute(reshape(tmp,[size(a,1),size(a,3),m(i)]),[1,3,2]);
end
b = reshape(b,prod(m),d);                        % bring result in correct shape
end

function [val,r,s,v,w]=indexKron(i,j,K1,K2)
p = size(K2,1);
q = size(K2,2);
r = ceil(i/p);
s = ceil(j/q);
v = i - p*(r-1);
w = j - q*(s-1);
val = K1(r,s)*K2(v,w);
end

function [ samples ] = mvnSampling(A_z,A_i_j,A_size, samples_count, m_max, sparcity)
tic
i = 1:sparcity:A_size; 
j = 1:sparcity:A_size; 
[i,j] = meshgrid(i,j);
i = i(:);
j = j(:);
d = sparse(i,j,1,A_size,A_size);
d = spdiags(ones(A_size,1),0,d);
toc
samples = sample(A_z,A_i_j,d,m_max,A_size,samples_count);
end

function [y] = sparse_inv_times_v(G_sp,v)
y = zeros(numel(v),1);
for i = 1:size(G_sp,1)
   js = find(G_sp(i,:));
   y(i) = (v(i) - G_sp(i,js)*y(js))/G_sp(i,i);
end
end

function [G_sp] = precond(A_i_j,Sparcity)
G_sp = sparse(size(Sparcity,1),size(Sparcity,2));
parfor i = 1:size(Sparcity,1)
   js = find(Sparcity(i,:));
   A_JJ = zeros(numel(js));
   pairs = VChooseK(js,2);
   for p = 1:size(pairs,1)
        p1 = pairs(p,1);
        p2 = pairs(p,2);
        i1 = binarySearch(js,p1);
        i2 = binarySearch(js,p2);
        a = A_i_j( p1, p2 );
        A_JJ(i1,i2)=a;
        A_JJ(i2,i1)=a;
   end
   for j = 1:numel(js)
       A_JJ(j,j) = A_i_j(js(j),js(j));
   end
   opts = struct();
   e_mi = zeros(size(js));
   e_mi(end) = 1;
   opts.SYM = true; opts.POSDEF = true;
   v = linsolve(A_JJ,e_mi',opts);
   if v(end) < 0 
       fid = fopen('log.txt', 'a+');
       while fid == -1
            pause(0.1);
            fid = fopen('log.txt', 'a+');
       end
       formatSpec = '%8.3f,';
       fprintf(fid,'Vec: ');
       fprintf(fid,formatSpec,v);
       fprintf(fid,'\nPossdef: %8.3f\n',v'*A_JJ*v);
       fprintf(fid,'Solqual: ');
       fprintf(fid,formatSpec,A_JJ*v);
       fprintf(fid,'\n');
   end  
   v = v/sqrt(v(end));
   sparse_line = sparse(1,js,v,1,size(Sparcity,2)); % Needed by parfor
   G_sp(i,:) = sparse_line; 
end
end


function [samples]=sample(A_z,A_i_j,Sparcity,max_m,A_size,samples_count)
tic
G_sp = precond(A_i_j,Sparcity);
toc
disp('Precond_end');
save('G_sp','G_sp');
A_z_precond = @(z) G_sp*A_z(G_sp'*z);
samples = zeros(samples_count,A_size);
for i = 1:samples_count 
    tic
    samples(i,:) = solve(A_z_precond, randn(A_size,1),max_m);
    toc
    disp('Sample_end');
    save('sample','samples');
    tic
    samples(i,:) = sparse_inv_times_v(G_sp,samples(i,:));
    toc
    disp('inv_precond_end');
    save('sample','samples');
end
end


function [Lz,m]=solve(A_z,z,m)
[a,b,Vm]=Lanczos(m,A_z,z/norm(z));
m = numel(a);
Tm = diag(b(2:end-1), 1) + diag(b(2:end-1), -1) + diag(a);
sqrtTm = sqrtm(Tm)';
e1 = zeros(size(Tm,2),1);
e1(1) =  1;
Lz = norm(z).*Vm*sqrtTm*e1;
end

function [alpha,beta,v]=Lanczos(jmax,A_z,v1)
reps=10*sqrt(eps);
eps1=eps;
nconv=0;
lmbc=0;
r=v1;
v=[];
alpha=[];
beta=[];
wjps=[];
beta(1)=norm(r);
sqn=1;
bijn=1;
for j=1:jmax,
  % Basic recursion
  v(:,j)=r/beta(j);
  r=A_z(v(:,j));
  if j>1, r=r-v(:,j-1)*beta(j); end;
  alpha(j)=r'*v(:,j);
  r=r-v(:,j)*alpha(j);
  beta(j+1)=norm(r);
  % Reorthogonalize
  h1=v'*r;
  r=r-v*h1;
  nh1=norm(h1);
  wjps(j)=nh1;
  if nh1>reps,
     r=r-v*(v'*r);
  end;
  
    % Compute Ritz value and test for convergence or stagnation
    Tj=diag(alpha)+diag(beta(2:j),1)+diag(beta(2:j),-1);
    [s, d]=eig(Tj);
    thetas=diag(d);
    bndv=abs(beta(j+1)*s(j,:));
    % Take Ritz vector closest to start vector
    [s1c it]=max(abs(s(1,:)));
    bndt=bndv(it);
    lambdat=d(it,it);
    bijo=bijn;
    bijn=bndt;
    sqo=sqn;
    sqn=abs(s(j,it)/s(1,it));
  if beta(j+1)<0.000001 | bndt<0.00000001 , break; end;
end;
lmb=d(it,it);
xv=v*s(:,it);
end
