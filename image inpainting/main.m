run('../gpml/startup.m')
pic_dir = '../Normalized Brodatz/';
list = dir(strcat(pic_dir,'*.tif'));
NCCs = zeros(100,length(list));
MSSIMs = zeros(100,length(list));
TSSs = zeros(100,length(list)); 
k_init = 1;
i_init = 1;
if exist('results.mat', 'file')
   file = matfile('results.mat');
   k_init = file.k;
   NCCs = file.NCCs;
   MSSIMs = file.MSSIMs;
   TSSs = file.TSSs;
   list = file.list;
   i_init = file.i+1;
   if i_init > 20
       k_init = k_init + 1;
       i_init = 1;
   end
end
for k = k_init:length(list)
    file = list(k).name;
    img = im2double(imread(strcat(pic_dir,file)));
    img = imresize(img,0.5);
    test_set = img(size(img,1)/2:end,size(img,2)/2:end); % Bottom half = testing set
    startX = randi([1, size(img,1)/2-76],20,1); % Top half = training set
    startY = randi([1, size(img,2)/2-76],20,1);
    for i = i_init:numel(startX)
        patch = img(startX(i):startX(i)+75,startY(i):startY(i)+75);
        j = 1;
        while j < 6
            try
              t1 = tic;  
              disp(['Starting computation of Picture#',num2str(k),' Patch#',num2str(i), ' Init#', num2str(j)])
              mask = ones(size(patch));
              mask(12:65,12:65) = NaN;
              [y_pred,learned] = texture_synt( patch, mask, struct('Q',40, 'inits', 150, 'iters', 10) );
              y_test = patch(isnan(mask));
              y_res = y_pred(isnan(mask));
              NCCs((i-1)*5+j,k) = y_test'*y_res / sqrt(sum(y_test.^2)*sum(y_res.^2)) ;
              MSSIMs((i-1)*5+j,k) = ssim(y_res,y_test);
              TSSs((i-1)*5+j,k) = max(max(normxcorr2(y_pred, test_set)));
              time = toc(t1);
              disp(['Overall time : ',num2str(time),' sec']);
              j = j + 1;
            catch
              disp('Warning bad init');
            end
        end
        save('results','NCCs','i','k','list','MSSIMs','TSSs')
        imwrite(y_pred,['results/figure' num2str(k) '_' num2str(i) '.png'])
        imwrite(gaussian_viz(learned),['results/figure' num2str(k) '_' num2str(i) '_spectre.png'])
        i_init = 1;
    end
end
exit;