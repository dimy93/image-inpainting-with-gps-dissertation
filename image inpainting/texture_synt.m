function [ ypred, learned, hyper ] = texture_synt( img, mask, opts )
%TEXTURE_SYNT Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3 opts = {}; end
if ~isfield(opts, 'Q') opts.Q = 20; end % Number of basis 
if ~isfield(opts, 'cg_maxit') opts.cg_maxit = 500; end % Number of iterations for LCG
if ~isfield(opts, 'cg_tol') opts.cg_tol = 1e-4; end % Tolerance for the LCG
if ~isfield(opts, 'inits') opts.inits = 100; end  % Number of different hyperparam inits
if ~isfield(opts, 'iters') opts.iters = 700; end  % Number of different hyperparam inits


%% Creating normalized training data
idx = find(~isnan(mask));
y = img(idx); 
my = mean(y);
sy = std(y);
y = ((y - my) ./ sy);
xg = {(1:size(img,1))',(1:size(img,2))'};
x = covGrid('expand',xg);
N = size(x,1);

%% Initialization
sn = .1*mean(abs(y));  % Initial variance
hyp.lik = log(sn);

gpmean = {@meanConst}; 
hyp.mean=mean(y);

cov = {{'covSMfast',opts.Q},{'covSMfast',opts.Q}};  % 1D SM kernel for each input dimension, with Q components
covg = {@covGrid,cov,xg};  % Kroneker matrix composed from input dimentions stored in cov


inf_method = @(varargin) infGrid(varargin{:},opts); % Kronker matrix inference
lik = @likGauss; % Gaussian likelihood

tic;
hyp = spectral_init(inf_method,hyp,gpmean,lik,cov,covg,x,y,idx,opts.inits);
time = toc;
disp(['Initialization time : ',num2str(time),' sec']);

%% Training params

p.length =    -opts.iters;
p.method =    'BFGS';
p.SIG = 0.1;
p.verbosity = 1; 
          
tic;  
params = minimize_new(hyp,@gp,p,inf_method,gpmean,covg,lik,idx,y);
time = toc;
disp(['Hyperparameter optimization time : ',num2str(time),' sec']);
tic;
postg = infGrid(params, gpmean, covg, lik, idx, y, opts);
time = toc;
disp(['Inference time : ',num2str(time),' sec']);

postg.L = @(x) 0*x;
star_ind = (1:N)';

%% Inference
if (numel(idx) ~= numel(img) )
    tic;
    ypred = gp(params, @infGrid, gpmean, covg, [], idx, postg, star_ind);
    time = toc;
    disp(['GP mean and variance calc time : ',num2str(time),' sec']);
    ypred = ypred*sy + my;
    ypred = reshape(ypred,size(img));
else
    ypred = zeros(0);
end
%% Report learned params
hyper = struct('cov',{covg},'mean',my,'stdev',sy, 'hyper', params);
params = params.cov;
wx = exp(params(1:opts.Q));                                             % mixture weights
mx = exp(reshape(params(opts.Q+(1:opts.Q)),1,opts.Q));                          % mixture centers
vx = exp(reshape(2*params(opts.Q*2+(1:opts.Q)),1,opts.Q)); 

wy = exp(params(opts.Q*3+(1:opts.Q)));                                             % mixture weights
my = exp(reshape(params(opts.Q*4+(1:opts.Q)),1,opts.Q));                          % mixture centers
vy = exp(reshape(2*params(opts.Q*5+(1:opts.Q)),1,opts.Q)); 
learned = struct('wx',wx,'wy',wy,'mx',mx,'my',my,'vx',vx,'vy',vy);
end

