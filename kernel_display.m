function isokernel_display( f, params, hyperparams )
K = feval(f,params,hyperparams);
cmap = colormap;
grid on
for i = 1:size(hyperparams,1)
    covariances = K(i,1,:);
    covariances = covariances(:);
    plot(params',covariances,'MarkerEdgeColor',cmap(round(size(cmap,1)/size(hyperparams,1)*i),:));
    hold on
end
figure
for i = 1:size(hyperparams,1)
    K_i = squeeze(K(i,:,:));
    K_i = (K_i + K_i')./2;
    f = mvnrnd(repmat(0,size(K_i,1),1),K_i+0.00001*eye(size(K_i)));
    plot(params',f','MarkerEdgeColor',cmap(round(size(cmap,1)/size(hyperparams,1)*i),:));
    hold on
end
end

