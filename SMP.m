function [ res ] = SMP( params, hyperparams )
%Hyperparams: NumConfigs x 3 x Number_of_Basis
ws = hyperparams(:,1,:);
ms = hyperparams(:,2,:);
ss = hyperparams(:,3,:);
for i = 1:size(ws,1)
    res(i,1:size(params,2),1:size(params,2)) = covSMfast(size(ws,3),[log(ws(i,:)),log(ms(i,:)),log(sqrt(ss(i,:)))],params');
end
end
