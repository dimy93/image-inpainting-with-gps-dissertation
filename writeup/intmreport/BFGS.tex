\section{The Newton method}	
The BFGS algorithm derivations to follow are based upon the material presented in ~\cite{nocedal2006numerical}. We loosely follow the organisation of  ~\cite{nocedal2006numerical}, deriving BFGS as a special case of the Quasi-Newton family of algorithms which are in turn derived as computational optimization of the Newton method. For the rest of this subsection the Newton method is presented similarly to ~\cite[Chapter~2.2]{nocedal2006numerical}. The Newton method is an iterative algorithm for function optimisation that given initial input location $x_0$ and function $f$, each iteration computes a point $x_i$, such that $f(x_{i}) \geq f(x_{i+1})$ for all $i\geq0$. The Newton method assumes $f$ is two-times differentiable and is guaranteed to converge to a local minimum of the function. The Newton method uses the  second-order Taylor series approximation:
\begin{equation}\label{eq:taylor}
f(x)\approx f(a)+\nabla f(a)(x-a) + \dfrac{1}{2}(x-a)^{T}(\nabla^{2}f(a))(x-a) \mbox{, as } x\to a
\end{equation}
Substituting $x_{n+1} = x_{n}+\Delta x$ for $x$ and $x_{n}$ for $a$ in Equation~\ref{eq:taylor} one can obtain:
\begin{equation}
f(x_{n+1})\approx f(x_{n})+\nabla f(x_{n})\Delta x + \dfrac{1}{2}\Delta x^{T}(\nabla^{2}f(x_{n}))\Delta x \mbox{, as } \Delta x\to 0
\end{equation}
For notational convenience $h_{n}(\Delta x) =  f(x_{n})+\nabla f(x_{n})\Delta x + \dfrac{1}{2}\Delta x^{T}(\nabla^{2}f(x_{n}))\Delta x$. To minimize $f$ it is reasonable at each iteration $n$ to choose the direction $\Delta x$ in which $f$ decreases most rapidly. This is hard to achieve for arbitrary function $f$. Knowing $f(x_{n+1})\approx h_{n}(\Delta x)$ for small enough $\Delta x$, one can select the direction $\Delta x$ in which $h_{n}$ decreases most rapidly, instead. One can obtain that direction analytically by differentiating $h_{n}$:
\begin{equation}
\Delta x = -H_{n}^{-1}g_{n}
\end{equation}, where $H_{n} = \nabla^{2}f(x_{n})$ is the Hessian matrix of $f$ and $g_n = \nabla f(x_{n})$ is the gradient of $f$. Since $f(x_{n+1})\approx h_{n}(\Delta x)$ only for small enough $\Delta x$, one usually uses only the direction of the vector $\Delta x$ to come up with update for $x_{n+1}$:
\begin{align}
x_{n+1} = x_{n}-\alpha(H_{n}^{-1}g_{n}) \mbox{, where}\\
\alpha = \min_{\alpha \geq 0} f(x_{n} - \alpha H_{n}^{-1}g_{n})\label{eq:linesearch}
\end{align}
Equation~\ref{eq:linesearch} is usually solved via line search methods. It is recommended that the line search obeys the Wolfe conditions~\cite[Chapter~6.1]{nocedal2006numerical}. The complete algorithm is given in Algorithm~\ref{alg:newton} below:
\begin{algorithm}
\caption{Newton method}
\begin{algorithmic}[1]
\Function{NewtonMethod}{$f,x_{0}$}
\For{$n = 0,1, \dots $} \Comment{until converged}
\State Calculate $g_{n}, H_{n}^{-1}$
\State $d = H_{n}^{-1}g_{n}$
\State $\alpha = \min_{\alpha \geq 0} f(x_{n} - \alpha d)$
\State$x_{n+1} = x_{n}-\alpha d$
\EndFor
\EndFunction
\end{algorithmic}
\label{alg:newton} 
\end{algorithm}
\section{Quasi-Newton methods}
The Newton method requires the calculation of the inverse Hessian matrix $H_n^{-1}$ of $f$ each iteration. That imposes $\mathcal{O}(nm^3)$ computational cost and $\mathcal{O}(m^2)$ memory storage for the Newton method where $m$ is the number of input dimensions of $f$. For functions whose input is highly multidimensional like the marginal likelihood functions presented in this document that makes the Newton method computationally impractical or even infeasible. The Quasi-Newton methods trade the quadratic convergence rate of the Newton method for computational gains. The resulting algorithms are very efficient to compute and still achieve sub-linear convergence rate~\cite[Chapter~2.2]{nocedal2006numerical}. That is achieved by maintaining a Hessian approximation that the Quasi-Newton family of algorithms updates every iteration by adding some low-rank matrix. The Quasi-Newton algorithms depend on the Secant Condition to choose their low-rank matrix update. The Secant Condition is derived by imposing the condition that the gradients of $f(x_{n+1})$ and $h_n(\Delta x)$ must agree at $x_{n}$ and $x_{n-1}$:
\begin{equation}\label{eq:seccond}
\begin{split}
&\nabla h_{n}(x_{n})=g_{n} \\
&\nabla h_{n}(x_{n-1})=g_{n-1}
\end{split}
\end{equation}

Solving Equation~\ref{eq:seccond} results in:
\begin{equation}\label{eq:seccondm}
\begin{split}
H_{n}^{-1} y_{n}   = s_{n} \mbox{, where}\\  
s_{n} = x_{n+1}-x_{n}\\
y_{n} = g_{n+1}-g_{n}
\end{split}
\end{equation}
As evident from Equation~\ref{eq:seccondm}, any Quasi-Newton Hessian update depends on the quantities $s_{n} = x_{n+1}-x_{n}$ and $y_{n} = g_{n+1}-g_{n}$. The exact form of the Hessian update differs from one Quasi-Newton algorithm to another but it is usually dependent only on the Hessian approximation in the previous step $H_{n-1}$, as well as $s_{n}$ and $y_{n}$. The high level pseudocode for all Quasi-Newton algorithms is given in Algorithm~\ref{alg:quasinewton} below:
\begin{algorithm}
\caption{Quasi-Newton methods}
\begin{algorithmic}[1]
\Function{Quasi-Newton}{$f,x_{0},H_{0}^{-1},$}
\State $g_{0} = \nabla f(x_{0})$
\For{$n = 0,1, \dots $} \Comment{until converged}
\State $d = H_{n}^{-1}g_{n}$
\State $\alpha = \min_{\alpha \geq 0} f(x_{n} - \alpha d)$
\State $x_{n+1} = x_{n}-\alpha d$
\State $g_{n+1} = \nabla f(x_{n+1})$
\State $s_{n+1} = x_{n+1} - x_{n}$
\State $y_{n+1} = g_{n+1} - g_{n}$
\State $H_{n+1}^{-1}$ = \Call{Quasi-Update}{$H_{n}^{-1},s_{n+1}, y_{n+1}$}
\EndFor
\EndFunction
\end{algorithmic}
\label{alg:quasinewton} 
\end{algorithm}
\section{BFGS}
As already discussed, BFGS is a Quasi-Newton method. Thus, to fully specify it one needs to choose a Hessian updating policy satisfying the Secant Condition. The Secant Condition is not enough to obtain an update policy by itself, since $H_{n+1}^{-1}$ is undetermined and therefore additional restrictions should be posed. To overcome the problem the BFGS creators adopted the smallest change principle that prefers solutions with smaller update cost. The update cost is measured in terms of the weighted Frobenius distance measure between $H_{n+1}^{-1}$  and $H_{n}^{-1}$. Since the inverse of any Hessian matrix is symmetric, the BFGS algorithm chooses to enforce $H_{n+1}^{-1}$ to be symmetric, as well to arrive at the following system:
\begin{equation}\label{eq:BFGSsys}
\begin{split}
H_{n+1}^{-1} = argmin \hspace{0.5em} \| H_{n+1}^{-1}- H_{n}^{-1}\|^2 \\
\mbox{s.t. } \hspace{0.5em}  H_{n+1}^{-1} y_{n} = s_{n} \\
            \hspace{0.5em}  H_{n+1}^{-1} \mbox{ is symmetric }
            \end{split}
\end{equation}
System~\ref{eq:BFGSsys} has a unique solution resulting in our update policy below:
\begin{equation}
\begin{split}
H_{n+1}^{-1} = (I - \rho_{n}s_{n}y_{n}^{T}) H_{n}^{-1} (I -  \rho_{n}y_{n}s_{n}^{T}) +  \rho_{n}s_{n}s_{n}^{T} \mbox{, where}\\
\rho_{n} = \dfrac{1}{y_{n}^{T}s_{n}}
\end{split}
\end{equation}
