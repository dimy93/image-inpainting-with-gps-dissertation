function [ res ] = SE( params, hyperparams )
for i = 1:size(hyperparams,1)
    res(i,1:size(params,2),1:size(params,2)) = covRQiso(log(hyperparams(i,:)),params');
end
end